#import <React/RCTBridgeModule.h>
#import <AdSupport/ASIdentifierManager.h>
#import <AppTrackingTransparency/ATTrackingManager.h>

@interface AdadaptedReactNativeSdk : NSObject <RCTBridgeModule>

@end
