# AdAdapted React Native SDK

The `AdAdapted React Native SDK` allows for implementation of the AdAdapted Ad Platform into your React Native project.

## Documentation

Please visit the following site to view all documentation for the AdAdapted SDK:

[AdAdapated SDK Docs](https://docs.adadapted.com/#/)
